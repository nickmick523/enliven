<table align="center"><tr><td align="center" width="9999">
<img src="https://cdn.discordapp.com/avatars/606760964183949314/e87c138f296e53e63fd55114eff36dcf.png" align="center" alt="Project icon">

# Enliven

  <p align="center">
  
  [![Discord Bots](https://top.gg/api/widget/status/606760964183949314.svg)](https://top.gg/bot/606760964183949314)
  [![Discord Bots](https://top.gg/api/widget/upvotes/606760964183949314.svg)](https://top.gg/bot/606760964183949314)
  [![Discord Bots](https://top.gg/api/widget/owner/606760964183949314.svg)](https://top.gg/bot/606760964183949314)
  
  </p>

> Powerful music and logging bot for Discord  

**Fully free** [Discord](https://discord.com/) bot with Embed music playback, emoji control, various music sources (including livestreams) and more.  
The best opportunities for logging messages.
</td></tr>
<tr><td align="center" width="9999">

#### Join our support server:

[![Discord support server](https://discordapp.com/api/guilds/706098979938500708/widget.png?style=banner3)](https://discord.gg/zfBffbBbCp)

</td></tr>
</table>

## Features
 * Music player
   * Embed playback control
     * Emoji control
     * Progress display
     * Queue display
     * Request history
     * This and more in one control message:  
     <img src="https://gitlab.com/skprochlab/nJMaLBot/-/wikis/uploads/80b7037668f4762bcb01017aa7114264/Screenshot_5.png" />
   * Various music sources
     * Youtube, Twitch, SoundCloud and all [supported by youtube-dl sites](https://ytdl-org.github.io/youtube-dl/supportedsites.html)
     * Spotify
     * Live streams
   * Bass boost support
   * Custom playlists
   * Multiple player nodes
 * Logging
   * Collects a full changes history
   * With attachment links
   * Display all message changes in one place
   * Fully configurable
   * Export changes history to image
     * Highlighting deletions and additions
     * Full support for rendering:
       * Emote
       * Custom emoji
       * Links
       * Mentions
     * [Example result (warning: *large image*)](https://cdn.discordapp.com/attachments/667271058461687809/722864116741439629/History-672479528634941440-722861553564778588.png)
 * Multilingual

## Getting started

1. You need to add the bot to your server using [this link](https://discordapp.com/oauth2/authorize?client_id=606760964183949314&scope=bot&permissions=1110764608)
2. To find out all available commands use `&help`

### Initial Configuration

* The default prefix for all commands is `&` (*or bot user mention*) by default. Set a custom server prefix with the `setprefix` command!  
* You can choose the language for the bot using the `language` command.  
* You can set up message logging using the `logging` command. 

## Contributing

If you'd like to contribute, please fork the repository and use a feature
branch. Pull requests are warmly welcome.

## Links

- [Project homepage](https://gitlab.com/skprochlab/nJMaLBot)
- [Repository](https://gitlab.com/skprochlab/nJMaLBot)
- [Issue tracker](https://gitlab.com/skprochlab/nJMaLBot/-/issues)
- [Discord support server](https://discord.gg/zfBffbBbCp)  
- Enliven on discord bots lists:
  - [top.gg](https://top.gg/bot/606760964183949314)
  - [discordbotlist.com](https://discordbotlist.com/bots/enliven)
  - [bots.ondiscord.xyz](https://bots.ondiscord.xyz/bots/606760964183949314)
  - [discord.bots.gg](https://discord.bots.gg/bots/606760964183949314)
  
## Help us
You can help by making a contributing or by voting on the following sites:
- [top.gg](https://top.gg/bot/606760964183949314) (vote every 12 hours, write a review)
- [discordbotlist.com](https://discordbotlist.com/bots/enliven) (vote every 12 hours)
- [bots.ondiscord.xyz](https://bots.ondiscord.xyz/bots/606760964183949314) (write a review)
