﻿namespace Common.Config {
    public interface IPrefixProvider {
        string GetPrefix();
    }
}