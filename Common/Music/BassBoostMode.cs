﻿namespace Common.Music {
    public enum BassBoostMode {
        Off,
        Low,
        Medium,
        High,
        Extreme
    }
}