﻿using System.Threading.Tasks;

namespace Common {
    public interface IService {
        Task Initialize();
    }
}